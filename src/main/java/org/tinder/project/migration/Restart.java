package org.tinder.project.migration;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Restart {

    private final static String URL = "jdbc:postgresql://tinderdb.chryzir8jj1b.us-east-1.rds.amazonaws.com:5432/tinderdb";
    private final static String NAME = "postgres";
    private final static String PASSWORD = "sanan123*";

    public static void main(String[] args) {
        DbSetup.migrate(URL,NAME,PASSWORD);
        log.warn("Restarted your database");
    }

}