package org.tinder.project.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class DBConnection {

    private final static String URL = "jdbc:postgresql://tinderdb.chryzir8jj1b.us-east-1.rds.amazonaws.com:5432/tinderdb";
    private final static String NAME = "postgres";
    private final static String PASSWORD = "sanan123*";

    private static Connection connection;

    private DBConnection(){
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(URL,NAME,PASSWORD);
            } catch (SQLException e) {
                log.error("Can't connected to database");
            }
        }
        return connection;
    }
}
