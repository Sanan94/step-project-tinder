-- auto-generated definition
create table likes
(
    id        serial not null
        constraint likes_pk
            primary key,
    user_from integer,
    user_to   integer
);

CREATE ROLE d79pqhfsbv3cgk;

alter table likes
    owner to d79pqhfsbv3cgk;

create unique index likes_id_uindex
    on likes (id);

